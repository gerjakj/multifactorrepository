﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace ProjectMultiFactor.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [DataType(DataType.Text)]
        [Display(Name = "Full name")]
        public string Name { get; set; }

        public double WorkloadIndex { get; set; }

        [Display(Name = "Birth Date")]
        [DataType(DataType.Date)]
        public DateTime Birth { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        public List<Entities.Task> Tasks { get; set; }

        public ApplicationUser()
        {
            Tasks = new List<Entities.Task>();
        }
    }
}
