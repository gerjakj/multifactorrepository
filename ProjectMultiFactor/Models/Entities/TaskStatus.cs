﻿namespace MultiFactorProject.Models.Entities
{
    public enum TaskStatus
    {
        Opened, Active, Closed, Pending
    }
}