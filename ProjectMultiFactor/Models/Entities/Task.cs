﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectMultiFactor.Models.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public ApplicationUser ResponsibleUser { get; set; }
        public string ResponsibleUserId { get; set; }
        public string Name { get; set; }
        public DateTime Deadline { get; set; }
        public TaskStatus? Status { get; set; }
        public int Priority { get; set; }
        public int Complexity { get; set; }
        public DateTime CreationDate { get; set; }
        public List<Comment> Comments { get; set; }

        public Task()
        {
            Comments = new List<Comment>();
        }
    }
}
