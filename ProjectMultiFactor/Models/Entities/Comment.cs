﻿using System;
using System.Threading.Tasks;

namespace ProjectMultiFactor.Models.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public ApplicationUser Author { get; set; }
        public DateTime CreationDate { get; set; }
        public String Text { get; set; }
        public int TaskId { get; set; }
        public Task Task { get; set; }
    }
}
